if(`test -n "-lxml2 -lz -llzma -licui18n -licuuc -licudata -lm"`) then

if(${?LD_LIBRARY_PATH}) then
    setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:-lxml2 -lz -llzma -licui18n -licuuc -licudata -lm
else
   setenv LD_LIBRARY_PATH -lxml2 -lz -llzma -licui18n -licuuc -licudata -lm
endif

endif

Package: tabula
Title: Analysis and Visualization of Archaeological Count Data
Version: 2.0.0
Authors@R: 
    c(person(given = "Nicolas",
             family = "Frerebeau",
             role = c("aut", "cre"),
             email = "nicolas.frerebeau@u-bordeaux-montaigne.fr",
             comment = c(ORCID = "0000-0001-5759-4944", 
                         affiliation = "Université Bordeaux Montaigne")),
      person(given = "Brice",
             family = "Lebrun",
             role = "ctb",
             email = "brice.lebrun@u-bordeaux-montaigne.fr",
             comment = c(ORCID = "0000-0001-7503-8685", 
                         affiliation = "Université Bordeaux Montaigne")),
      person(given = "Matthew",
             family = "Peeples",
             role = "ctb",
             email = "matthew.peeples@asu.edu",
             comment = c(ORCID = "0000-0003-4496-623X", 
                         affiliation = "Arizona State University")),
      person(given = "Ben",
             family = "Marwick",
             role = "ctb",
             email = "bmarwick@uw.edu",
             comment = c(ORCID = "0000-0001-7879-4531", 
                         affiliation = "University of Washington")),
      person(given = "Anne",
             family = "Philippe",
             role = "ctb",
             email = "anne.philippe@univ-nantes.fr",
             comment = c(ORCID = "0000-0002-5331-5087", 
                         affiliation = "Université de Nantes")),
      person(given = "Jean-Baptiste",
             family = "Fourvel",
             role = "ctb",
             email = "fourvel@mmsh.univ-aix.fr",
             comment = c(ORCID = "0000-0002-1061-4642", 
                         affiliation = "CNRS")))
Maintainer: Nicolas Frerebeau <nicolas.frerebeau@u-bordeaux-montaigne.fr>
Description: An easy way to examine archaeological count data. This
    package provides several tests and measures of diversity:
    heterogeneity and evenness (Brillouin, Shannon, Simpson, etc.),
    richness and rarefaction (Chao1, Chao2, ACE, ICE, etc.), turnover and
    similarity (Brainerd-Robinson, etc.). The package make it easy to
    visualize count data and statistical thresholds: rank vs abundance
    plots, heatmaps, Ford (1962) and Bertin (1977) diagrams.
License: GPL (>= 3)
URL: https://packages.tesselle.org/tabula/,
        https://github.com/tesselle/tabula
BugReports: https://github.com/tesselle/tabula/issues
Depends: R (>= 3.3)
Imports: arkhe (>= 1.0.0), ggplot2, methods, rlang, stats, utils
Suggests: covr, folio, khroma, knitr, rmarkdown, testthat (>= 3.0.0),
        vdiffr (>= 1.0.0)
VignetteBuilder: knitr
Config/testthat/edition: 3
Encoding: UTF-8
RoxygenNote: 7.2.1
Collate: 'AllClasses.R' 'AllGenerics.R' 'coerce.R' 'data.R'
        'deprecate.R' 'ggplot2.R' 'index_diversity.R'
        'index_heterogeneity.R' 'index_rarefaction.R'
        'index_richness.R' 'index_similarity.R' 'index_test.R'
        'index_turnover.R' 'matrigraph.R' 'mutators.R' 'plot_bertin.R'
        'plot_diceleraas.R' 'plot_diversity.R' 'plot_ford.R'
        'plot_heatmap.R' 'plot_rank.R' 'plot_spot.R' 'reexport.R'
        'seriograph.R' 'show.R' 'statistics.R' 'subset.R'
        'tabula-package.R' 'utilities.R' 'validate.R' 'zzz.R'
LazyData: true
NeedsCompilation: no
Packaged: 2022-11-06 13:32:26 UTC; nicolas
Author: Nicolas Frerebeau [aut, cre] (<https://orcid.org/0000-0001-5759-4944>,
    Université Bordeaux Montaigne),
  Brice Lebrun [ctb] (<https://orcid.org/0000-0001-7503-8685>, Université
    Bordeaux Montaigne),
  Matthew Peeples [ctb] (<https://orcid.org/0000-0003-4496-623X>, Arizona
    State University),
  Ben Marwick [ctb] (<https://orcid.org/0000-0001-7879-4531>, University
    of Washington),
  Anne Philippe [ctb] (<https://orcid.org/0000-0002-5331-5087>,
    Université de Nantes),
  Jean-Baptiste Fourvel [ctb] (<https://orcid.org/0000-0002-1061-4642>,
    CNRS)
Repository: CRAN
Date/Publication: 2022-11-06 15:30:02 UTC
Built: R 4.2.3; ; 2023-05-04 09:22:48 UTC; unix

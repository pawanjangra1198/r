Package: workflows
Title: Modeling Workflows
Version: 1.0.0
Authors@R: c(
    person("Davis", "Vaughan", , "davis@rstudio.com", role = c("aut", "cre")),
    person("RStudio", role = c("cph", "fnd"))
  )
Description: Managing both a 'parsnip' model and a preprocessor, such as a
    model formula or recipe from 'recipes', can often be challenging. The
    goal of 'workflows' is to streamline this process by bundling the
    model alongside the preprocessor, all within the same object.
License: MIT + file LICENSE
URL: https://github.com/tidymodels/workflows,
        https://workflows.tidymodels.org
BugReports: https://github.com/tidymodels/workflows/issues
Depends: R (>= 3.4)
Imports: cli (>= 3.3.0), generics (>= 0.1.2), glue (>= 1.6.2), hardhat
        (>= 1.2.0), lifecycle (>= 1.0.1), parsnip (>= 1.0.0), rlang (>=
        1.0.3), tidyselect (>= 1.1.2), vctrs (>= 0.4.1)
Suggests: butcher (>= 0.2.0), covr, dials (>= 1.0.0), knitr, magrittr,
        modeldata (>= 1.0.0), recipes (>= 1.0.0), rmarkdown, testthat
        (>= 3.0.0)
VignetteBuilder: knitr
Config/Needs/website: dplyr, ggplot2, tidyr, tidyverse/tidytemplate,
        yardstick
Config/testthat/edition: 3
Encoding: UTF-8
RoxygenNote: 7.2.0
NeedsCompilation: no
Packaged: 2022-07-05 14:46:30 UTC; davis
Author: Davis Vaughan [aut, cre],
  RStudio [cph, fnd]
Maintainer: Davis Vaughan <davis@rstudio.com>
Repository: CRAN
Date/Publication: 2022-07-05 15:20:02 UTC
Built: R 4.2.1; ; 2022-08-01 09:02:48 UTC; unix

Package: WRS2
Type: Package
Title: A Collection of Robust Statistical Methods
Version: 1.1-4
Date: 2022-06-09
Authors@R: c(person(given="Patrick", family="Mair", email="mair@fas.harvard.edu", role=c("cre","aut")),
             person(given="Rand", family="Wilcox", email="rwilcox@usc.edu", role=c("aut")),
             person(given="Indrajeet", family="Patil", email="patilindrajeet.science@gmail.com", role=c("ctb")))
Description: A collection of robust statistical methods based on Wilcox' WRS functions. It implements robust t-tests (independent and dependent samples), robust ANOVA (including between-within subject designs), quantile ANOVA, robust correlation, robust mediation, and nonparametric ANCOVA models based on robust location measures.
License: GPL-3
URL: https://r-forge.r-project.org/projects/psychor/
Imports: MASS, reshape, plyr, stats, graphics, grDevices, utils, mc2d
Depends: R (>= 3.2.0)
Suggests: knitr, car, ggplot2, colorspace, mediation, GGally, codetools
VignetteBuilder: knitr
LazyData: yes
LazyLoad: yes
ByteCompile: yes
NeedsCompilation: yes
Packaged: 2022-06-09 15:20:42 UTC; patrick
Author: Patrick Mair [cre, aut],
  Rand Wilcox [aut],
  Indrajeet Patil [ctb]
Maintainer: Patrick Mair <mair@fas.harvard.edu>
Repository: CRAN
Date/Publication: 2022-06-10 16:33:29 UTC
Built: R 4.2.1; ; 2022-08-06 17:50:01 UTC; unix
